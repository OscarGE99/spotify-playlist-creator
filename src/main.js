const CLIENT_ID = import.meta.env.VITE_CLIENT_ID;
const CLIENT_SECRET = import.meta.env.VITE_CLIENT_SECRET;
const REDIRECT_URI = import.meta.env.VITE_REDIRECT_URI; //https://playlist-creator-two.vercel.app
const AUTH_URL = import.meta.env.VITE_AUTH_URL;
const TOKEN_URL = import.meta.env.VITE_TOKEN_URL;
const API_BASE_URL = import.meta.env.VITE_API_BASE_URL;
const accessToken = await getAccessToken(CLIENT_ID, CLIENT_SECRET);

const fanName = document.getElementById("fan-name");
const searchInput = document.getElementById("song-search");
const songResults = document.getElementById("song-results");
const playlist = document.getElementById("playlist");
const numberSongs = document.getElementById("number-songs");
const holdragTxt = document.getElementById("holdrag-txt");
const nameOfPlaylist = document.getElementById("name-of-playlist");
const nameOfPlaylist2 = document.getElementById("name-of-playlist-2");
const step1Container = document.getElementById("step-1");
const step2Container = document.getElementById("step-2");
const step3Container = document.getElementById("step-3");
const step4Container = document.getElementById("step-4");
const step1Button = document.getElementById("step-1-button");
const step2Button = document.getElementById("step-2-button");
const back2button = document.getElementById("back-2-button");
const back3button = document.getElementById("back-3-button");
const tryAgainButton = document.getElementById("try-again-button");
const createButton = document.getElementById("create-button");
const link = document.getElementById("playlist-url");

let selectedSongs = [];
var playlistUrl = "";
const base64Image = await imageUrlToBase64("/My_Tour_Setlist.jpeg");

let draggedElement = null;
let touchStartY = 0;
let scrollInterval = null;

function showStep(stepElement) {
  stepElement.classList.remove("hide");
  stepElement.classList.add("show");
  stepElement.style.display = "flex";
}

function hideStep(stepElement) {
  stepElement.classList.remove("show");
  stepElement.classList.add("hide");
  setTimeout(() => {
    stepElement.style.display = "none";
  }, 300);
}

step1Button.addEventListener("click", function () {
  const fanNameValue = fanName.value.trim();
  if (fanNameValue === "") {
    Toastify({
      text: "⚠️ Please enter your name",
      duration: 5000,
      close: false,
      gravity: "bottom",
      position: "right",
      className: "warning",
      stopOnFocus: true,
      offset: {
        x: 10,
        y: 10,
      },
      style: {
        background: "linear-gradient(to right, #ffffff, #e7e7e7)",
        color: "#172951",
        fontFamily: "'Antarctican Mono', monospace",
        fontWeight: "800",
        borderRadius: "10px",
        boxShadow: "2px 3px 5px 2px rgba(213, 203, 203, 0.32)",
        textAlign: "center",
      },
      onClick: function () {},
    }).showToast();
  } else {
    hideStep(step1Container);
    setTimeout(() => showStep(step2Container), 300);
  }
});

step2Button.addEventListener("click", function () {
  if (selectedSongs.length < 3) {
    Toastify({
      text: "⚠️ Please select at least 3 songs",
      duration: 5000,
      close: false,
      gravity: "bottom",
      position: "right",
      className: "warning",
      stopOnFocus: true,
      offset: {
        x: 10,
        y: 10,
      },
      style: {
        background: "linear-gradient(to right, #ffffff, #e7e7e7)",
        color: "#172951",
        fontFamily: "'Antarctican Mono', monospace",
        fontWeight: "800",
        borderRadius: "10px",
        boxShadow: "2px 3px 5px 2px rgba(213, 203, 203, 0.32)",
        textAlign: "center",
      },
      onClick: function () {},
    }).showToast();
  } else {
    let playlistName =
      "Las Mujeres Ya No Lloran World Tour (" + fanName.value + "’s Setlist)";
    hideStep(step2Container);
    setTimeout(() => {
      showStep(step3Container);
      nameOfPlaylist.innerHTML = playlistName;
    }, 300);
  }
});

back2button.addEventListener("click", function () {
  hideStep(step2Container);
  setTimeout(() => showStep(step1Container), 300);
});

back3button.addEventListener("click", function () {
  hideStep(step3Container);
  setTimeout(() => showStep(step2Container), 300);
});

tryAgainButton.addEventListener("click", function () {
  hideStep(step4Container);
  setTimeout(() => showStep(step1Container), 300);
});

createButton.addEventListener("click", async function () {
  const accessTokenUser = localStorage.getItem("access_token_user");
  const expiresAt = localStorage.getItem("expires_at");

  if (!accessTokenUser || new Date().getTime() > expiresAt) {
    if (accessTokenUser) {
      await getRefreshToken();
    } else {
      startPlaylistCreationFlow();
    }
  } else {
    handleCallback();
  }
});

async function imageUrlToBase64(url) {
  const response = await fetch(url);
  const blob = await response.blob();
  return new Promise((resolve, reject) => {
    const reader = new FileReader();
    reader.onloadend = () => resolve(reader.result.split(",")[1]);
    reader.onerror = reject;
    reader.readAsDataURL(blob);
  });
}

searchInput.addEventListener("input", function () {
  const query = searchInput.value.trim();
  if (query.length > 0) {
    searchSongs(query);
  } else {
    songResults.style.display = "none";
  }
});

songResults.addEventListener("click", function (event) {
  event.stopPropagation();
  const selectedOption = event.target;
  if (selectedOption.tagName === "DIV") {
    addSongToPlaylist(
      selectedOption.textContent,
      selectedOption.getAttribute("data-value")
    );
    songResults.style.display = "none";
    searchInput.value = "";
    numberSongs.innerHTML = `SELECTED ${selectedSongs.length}/50`;
  }
});

document.addEventListener("click", function (event) {
  if (!songResults.contains(event.target) && event.target !== searchInput) {
    songResults.style.display = "none";
  }
});

function searchSongs(query) {
  fetch(
    API_BASE_URL +
      `search?q=artist:Shakira%20track:${query}&type=track&limit=20`,
    {
      headers: {
        Authorization: `Bearer ${accessToken}`,
      },
    }
  )
    .then((response) => response.json())
    .then((data) => {
      if (data.tracks && data.tracks.items) {
        let tracks = data.tracks.items;
        tracks = filterUniqueTracks(tracks);
        // tracks.sort((a, b) => a.name.localeCompare(b.name));
        displayResults(tracks);
      }
    })
    .catch((error) => console.error("Error fetching songs:", error));
}

function filterUniqueTracks(tracks) {
  const trackNames = new Set(selectedSongs.map((song) => song.title));
  return tracks.filter((track) => {
    if (trackNames.has(track.name)) {
      return false;
    } else {
      trackNames.add(track.name);
      return true;
    }
  });
}

function displayResults(tracks) {
  songResults.innerHTML = "";
  tracks.forEach((track) => {
    const option = document.createElement("div");
    option.setAttribute("data-value", track.id);
    option.textContent = track.name;
    songResults.appendChild(option);
  });
  songResults.style.display = "flex";
}

function addSongToPlaylist(name, id) {
  if (selectedSongs.length >= 50) {
    alert("You cannot add more than 50 songs to the playlist.");
    return;
  }
  const song = { title: name, id: id };
  if (!selectedSongs.some((s) => s.id === id)) {
    selectedSongs.push(song);
    updatePlaylist();
    numberSongs.innerHTML = `SELECTED ${selectedSongs.length}/50`;
  }
}

function updatePlaylist() {
  playlist.innerHTML = "";
  holdragTxt.style.display = selectedSongs.length >= 2 ? "block" : "none";
  selectedSongs.forEach((song, index) => {
    const listItem = document.createElement("li");
    listItem.setAttribute("data-id", song.id);
    listItem.setAttribute("data-index", index);
    listItem.setAttribute("draggable", true);

    const songContent = document.createElement("div");
    songContent.setAttribute("class", "song-content");

    const songNumber = document.createElement("span");
    songNumber.setAttribute("class", "song-number");
    songNumber.textContent = (index + 1).toString().padStart(2, "0") + ".";

    const dragIcon = createSVGIcon(
      "drag-icon",
      24,
      24,
      "<line x1='3' y1='12' x2='21' y2='12'></line><line x1='3' y1='6' x2='21' y2='6'></line><line x1='3' y1='18' x2='21' y2='18'></line>"
    );

    const songTitle = document.createElement("span");
    songTitle.setAttribute("class", "song-title");
    songTitle.textContent = song.title;

    songContent.appendChild(dragIcon);
    songContent.appendChild(songNumber);
    songContent.appendChild(songTitle);

    const removeButton = createRemoveButton(song.id);

    listItem.appendChild(songContent);
    listItem.appendChild(removeButton);
    playlist.appendChild(listItem);

    addDragAndDropHandlers(listItem);
  });
}

function createSVGIcon(className, width, height, innerHTML) {
  const svgIcon = document.createElementNS("http://www.w3.org/2000/svg", "svg");
  svgIcon.setAttribute("class", className);
  svgIcon.setAttribute("width", width);
  svgIcon.setAttribute("height", height);
  svgIcon.setAttribute("viewBox", "0 0 24 24");
  svgIcon.setAttribute("fill", "none");
  svgIcon.setAttribute("stroke", "currentColor");
  svgIcon.setAttribute("stroke-width", "2");
  svgIcon.setAttribute("stroke-linecap", "round");
  svgIcon.setAttribute("stroke-linejoin", "round");
  svgIcon.innerHTML = innerHTML;
  return svgIcon;
}

function createRemoveButton(songId) {
  const removeIcon = createSVGIcon(
    "",
    22,
    22,
    "<line x1='18' y1='6' x2='6' y2='18'></line><line x1='6' y1='6' x2='18' y2='18'></line>"
  );

  const removeButton = document.createElement("button");
  removeButton.setAttribute("class", "remove-button");
  removeButton.appendChild(removeIcon);
  removeButton.addEventListener("click", () => {
    selectedSongs = selectedSongs.filter((s) => s.id !== songId);
    updatePlaylist();
  });

  return removeButton;
}

function addDragAndDropHandlers(element) {
  element.addEventListener("dragstart", handleDragStart);
  element.addEventListener("dragover", handleDragOver);
  element.addEventListener("drop", handleDrop);

  element.addEventListener("touchstart", handleTouchStart, { passive: false });
  element.addEventListener("touchmove", handleTouchMove, { passive: false });
  element.addEventListener("touchend", handleTouchEnd, { passive: false });
}

function handleDragStart(e) {
  draggedElement = e.target;
  e.dataTransfer.effectAllowed = "move";
  e.dataTransfer.setData("text/html", e.target.innerHTML);
  e.target.style.opacity = "0.5";
}

function handleDragOver(e) {
  if (e.preventDefault) {
    e.preventDefault();
  }
  e.dataTransfer.dropEffect = "move";
  autoScroll(e.clientY);
  return false;
}

function handleDrop(e) {
  if (e.stopPropagation) {
    e.stopPropagation();
  }
  if (draggedElement !== e.target) {
    const draggedIndex = parseInt(
      draggedElement.getAttribute("data-index"),
      10
    );
    const targetIndex = parseInt(
      e.target.closest("li").getAttribute("data-index"),
      10
    );
    moveElement(draggedIndex, targetIndex);
  }
  clearInterval(scrollInterval);
  draggedElement.style.opacity = "";
  draggedElement = null;
  return false;
}

function handleTouchStart(e) {
  touchStartY = e.touches[0].clientY;
  draggedElement = e.target.closest("li");
  draggedElement.classList.add("dragging");
}

function handleTouchMove(e) {
  if (!draggedElement) return;

  const touchCurrentY = e.touches[0].clientY;
  const targetElement = document
    .elementFromPoint(e.touches[0].clientX, touchCurrentY)
    .closest("li");

  if (targetElement && targetElement !== draggedElement) {
    const draggedIndex = parseInt(
      draggedElement.getAttribute("data-index"),
      10
    );
    const targetIndex = parseInt(targetElement.getAttribute("data-index"), 10);
    moveElement(draggedIndex, targetIndex);
    draggedElement = playlist.querySelector(
      `li[data-id="${selectedSongs[targetIndex].id}"]`
    );
    draggedElement.classList.add("dragging");
  }

  autoScroll(touchCurrentY);
  e.preventDefault();
}

function handleTouchEnd(e) {
  if (draggedElement) {
    draggedElement.classList.remove("dragging");
    draggedElement = null;
  }
  clearInterval(scrollInterval);
  e.preventDefault();
}

function moveElement(fromIndex, toIndex) {
  const element = selectedSongs.splice(fromIndex, 1)[0];
  selectedSongs.splice(toIndex, 0, element);
  updatePlaylist();
}

function autoScroll(clientY) {
  const rect = playlist.getBoundingClientRect();
  if (clientY - rect.top < 50) {
    startScrolling(-5);
  } else if (rect.bottom - clientY < 50) {
    startScrolling(5);
  } else {
    clearInterval(scrollInterval);
  }
}

function startScrolling(speed) {
  if (scrollInterval) {
    clearInterval(scrollInterval);
  }
  scrollInterval = setInterval(() => {
    playlist.scrollTop += speed;
  }, 10);
}

async function startPlaylistCreationFlow() {
  localStorage.setItem("fan_name", fanName.value);
  localStorage.setItem("selected_songs", JSON.stringify(selectedSongs));
  redirectToAuthCodeFlow(CLIENT_ID);
}

async function handleCallback() {
  const params = new URLSearchParams(window.location.search);
  const code = params.get("code");
  const error = params.get("error");

  if (error) {
    console.error("Error during authorization:", error);
    return;
  }

  if (
    code &&
    localStorage.getItem("fan_name") &&
    localStorage.getItem("selected_songs")
  ) {
    nameOfPlaylist2.innerHTML =
      "Las Mujeres Ya No Lloran World Tour (" +
      localStorage.getItem("fan_name") +
      "’s Setlist)";

    hideStep(step1Container);
    setTimeout(() => showStep(step4Container), 300);

    const accessTokenUser = await getAccessTokenUser(CLIENT_ID, code);
    const expiresAt = new Date().getTime() + accessTokenUser.expires_in * 1000; // Calculate expiration timestamp

    localStorage.setItem("access_token_user", accessTokenUser.access_token);
    localStorage.setItem("refresh_token", accessTokenUser.refresh_token);
    localStorage.setItem("expires_at", expiresAt);

    if (accessTokenUser) {
      const profile = await fetchProfile(accessTokenUser.access_token);
      const playlist = await fetchCreatePlaylist(
        profile.id,
        accessTokenUser.access_token
      );
      await fetchAddTracks(playlist.id, accessTokenUser.access_token);
      await fetchAddCoverImage(playlist.id, accessTokenUser.access_token);
      link.textContent = playlist.external_urls.spotify;
      link.href = playlist.external_urls.spotify;
      link.target = "_blank";
    }
  } else if (localStorage.getItem("access_token_user")) {
    nameOfPlaylist2.innerHTML =
      "Las Mujeres Ya No Lloran World Tour (" +
      localStorage.getItem("fan_name") +
      "’s Setlist)";

    hideStep(step1Container);
    setTimeout(() => showStep(step4Container), 300);

    const accessTokenUser = localStorage.getItem("access_token_user");
    const profile = await fetchProfile(accessTokenUser);
    const playlist = await fetchCreatePlaylist(profile.id, accessTokenUser);
    await fetchAddTracks(playlist.id, accessTokenUser);
    await fetchAddCoverImage(playlist.id, accessTokenUser);
    link.textContent = playlist.external_urls.spotify;
    link.href = playlist.external_urls.spotify;
    link.target = "_blank";
  }
}

if (window.location.href.includes("/callback")) {
  handleCallback();
}

async function getRefreshToken() {
  const refreshToken = localStorage.getItem("refresh_token");

  const params = new URLSearchParams();
  params.append("client_id", CLIENT_ID);
  params.append("client_secret", CLIENT_SECRET);
  params.append("grant_type", "refresh_token");
  params.append("refresh_token", refreshToken);

  const result = await fetch(TOKEN_URL, {
    method: "POST",
    headers: { "Content-Type": "application/x-www-form-urlencoded" },
    body: params,
  });

  const newAccessToken = await result.json();
  const expiresAt = new Date().getTime() + newAccessToken.expires_in * 1000;

  localStorage.setItem("access_token_user", newAccessToken.access_token);
  localStorage.setItem(
    "refresh_token",
    newAccessToken.refresh_token || refreshToken
  );
  localStorage.setItem("expires_at", expiresAt);

  handleCallback();
}

async function fetchProfile(token) {
  const result = await fetch(API_BASE_URL + "me", {
    method: "GET",
    headers: { Authorization: `Bearer ${token}` },
  });

  return await result.json();
}

async function fetchCreatePlaylist(userId, token) {
  const playlistName =
    "Las Mujeres Ya No Lloran World Tour (" +
    localStorage.getItem("fan_name") +
    "’s Setlist)";
  const result = await fetch(API_BASE_URL + "users/" + userId + "/playlists", {
    method: "POST",
    headers: {
      Authorization: `Bearer ${token}`,
      "Content-Type": "application/json",
    },
    body: JSON.stringify({
      name: playlistName,
      description: "https://www.shakira.com",
      public: true,
    }),
  });
  if (!result.ok) {
    const errorDetails = await result.json();
    throw new Error(`Error creating playlist: ${errorDetails.error.message}`);
  }
  return await result.json();
}
async function fetchAddTracks(playlistId, token) {
  const uris = JSON.parse(localStorage.getItem("selected_songs")).map(
    (song) => `spotify:track:${song.id}`
  );
  const result = await fetch(
    API_BASE_URL + "playlists/" + playlistId + "/tracks",
    {
      method: "POST",
      headers: {
        Authorization: `Bearer ${token}`,
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        uris: uris,
      }),
    }
  );
  if (!result.ok) {
    const errorDetails = await result.json();
    throw new Error(
      `Error adding tracks to playlist: ${errorDetails.error.message}`
    );
  }
  return await result.json();
}

async function fetchAddCoverImage(playlistId, token) {
  const result = await fetch(
    API_BASE_URL + "playlists/" + playlistId + "/images",
    {
      method: "PUT",
      headers: {
        Authorization: `Bearer ${token}`,
        "Content-Type": "application/json",
      },
      body: base64Image,
    }
  );
  if (!result.ok) {
    const errorDetails = await result.text();
    throw new Error(`Error adding cover image to playlist: ${errorDetails}`);
  }

  return result.status;
}

async function getAccessToken(CLIENT_ID, CLIENT_SECRET) {
  const authOptions = {
    method: "POST",
    headers: {
      Authorization: "Basic " + btoa(CLIENT_ID + ":" + CLIENT_SECRET),
      "Content-Type": "application/x-www-form-urlencoded",
    },
    body: new URLSearchParams({
      grant_type: "client_credentials",
    }),
  };

  const result = await fetch(TOKEN_URL, authOptions)
    .then((response) => {
      if (!response.ok) {
        throw new Error("Network response was not ok " + response.statusText);
      }
      return response;
    })
    .catch((error) => {
      console.error("There was a problem with the fetch operation:", error);
    });

  const { access_token } = await result.json();
  return access_token;
}

async function redirectToAuthCodeFlow(clientId) {
  const verifier = generateCodeVerifier(128);
  const challenge = await generateCodeChallenge(verifier);

  localStorage.setItem("verifier", verifier);

  const params = new URLSearchParams();
  params.append("client_id", clientId);
  params.append("response_type", "code");
  params.append("redirect_uri", REDIRECT_URI);
  params.append(
    "scope",
    "user-read-private user-read-email playlist-modify-public ugc-image-upload"
  );
  params.append("code_challenge_method", "S256");
  params.append("code_challenge", challenge);

  document.location = AUTH_URL + `?${params.toString()}`;
}

async function getAccessTokenUser(clientId, code) {
  const verifier = localStorage.getItem("verifier");

  const params = new URLSearchParams();
  params.append("client_id", clientId);
  params.append("client_secret", CLIENT_SECRET);
  params.append("grant_type", "authorization_code");
  params.append("code", code);
  params.append("redirect_uri", REDIRECT_URI);
  params.append("code_verifier", verifier);

  const result = await fetch(TOKEN_URL, {
    method: "POST",
    headers: { "Content-Type": "application/x-www-form-urlencoded" },
    body: params,
  });

  const access_token_user = await result.json();
  return access_token_user;
}

function generateCodeVerifier(length) {
  let text = "";
  let possible =
    "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

  for (let i = 0; i < length; i++) {
    text += possible.charAt(Math.floor(Math.random() * possible.length));
  }
  return text;
}

async function generateCodeChallenge(codeVerifier) {
  const data = new TextEncoder().encode(codeVerifier);
  const digest = await window.crypto.subtle.digest("SHA-256", data);
  return btoa(String.fromCharCode.apply(null, [...new Uint8Array(digest)]))
    .replace(/\+/g, "-")
    .replace(/\//g, "_")
    .replace(/=+$/, "");
}
